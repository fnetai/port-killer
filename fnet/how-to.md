# @fnet/port-killer Developer Guide

## Overview

The `@fnet/port-killer` library is a simple Node.js utility designed to help developers terminate processes running on a specific port. This can be particularly useful during development when ports need to be freed for new processes. The library provides a straightforward public API that allows you to identify and terminate processes using specified ports on different operating systems.

## Installation

To install the `@fnet/port-killer` library, use either npm or yarn. Here are the installation commands:

### Using npm:
```bash
npm install @fnet/port-killer
```

### Using yarn:
```bash
yarn add @fnet/port-killer
```

## Usage

The primary functionality of the `@fnet/port-killer` library is encapsulated in its default exported function. Here’s how you can use it in your Node.js applications:

### Importing and Using the Library

```javascript
import killPort from '@fnet/port-killer';

// Specify the port number you wish to free
const portToKill = 3000;

// Attempt to kill the process using the specified port
killPort({ port: portToKill })
  .then(message => console.log(message))
  .catch(error => console.error(error));
```

This code snippet imports the `killPort` function from the library, specifies a port (e.g., 3000), and attempts to terminate any process currently using that port. If successful, it logs a success message; otherwise, it logs an error.

## Examples

Here are practical examples of using the `@fnet/port-killer` library:

### Kill a Process on Port 8080

```javascript
import killPort from '@fnet/port-killer';

killPort({ port: 8080 })
  .then(message => console.log(message)) // Output: "Process on port 8080 killed successfully." or "No process found on port 8080. Skipping kill command."
  .catch(error => console.error(error));
```

### Handle Errors Gracefully

```javascript
import killPort from '@fnet/port-killer';

killPort({ port: 1234 })
  .then(message => console.log(message))
  .catch(error => {
    // Handle specific errors or log them for debugging purposes
    console.error(`Failed to kill process: ${error}`);
  });
```

These examples demonstrate how the library is used to detect and terminate processes on specified ports, complete with error handling and messaging for different scenarios.

## Acknowledgement

We acknowledge the use of Node.js core modules in building this utility. The library's functionality handles platform-specific commands to achieve process termination across different operating systems.