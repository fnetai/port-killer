# @fnet/port-killer

This utility serves a straightforward purpose: it identifies and terminates any process running on a specified network port. It's useful for developers and system administrators who need to effortlessly free up ports for new applications or processes.

## How It Works

The tool checks your system for any running process that is using a defined port number. If it finds a process, it will terminate it. If no process is using the port, it will simply inform you that no action was taken.

## Key Features

- Detects processes running on a specified port.
- Terminates identified processes to free up the port.
- Compatible with Windows, macOS, and Linux operating systems.

## Conclusion

@fnet/port-killer is a handy tool to quickly check and clear ports on your system without needing to manually find or end processes, streamlining your workflow.