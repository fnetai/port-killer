import { exec } from 'node:child_process';
import os from 'node:os';

/**
 * Kills the process using the specified port, if found. Otherwise, indicates no process found.
 *
 * @param {Object} args - The arguments object.
 * @param {number} args.port - The port number to check and kill the process for.
 * @returns {Promise<string>} - A promise that resolves to a message indicating the action taken.
 */
export default (args) => {
  const { port } = args;

  return new Promise((resolve, reject) => {
    let command, parseCommandOutput;

    switch (os.platform()) {
      case 'win32':
        command = `netstat -ano | findstr :${port}`;
        parseCommandOutput = output => {
          const lines = output.trim().split('\n');
          const line = lines.find(line => line.trim().endsWith('LISTENING'));
          return line ? line.trim().split(/\s+/).pop() : null;
        };
        break;
      case 'darwin':
      case 'linux':
        command = `lsof -i tcp:${port} | awk 'NR!=1 {print $2}'`;
        parseCommandOutput = output => output.trim() ? output.trim().split('\n')[0] : null;
        break;
      default:
        return reject('Unsupported platform');
    }

    exec(command, (err, stdout, stderr) => {
      if (err || stderr) return reject(`Error: ${err || stderr}`);
      const pid = parseCommandOutput(stdout);
      if (!pid) return resolve(`No process found on port ${port}. Skipping kill command.`);

      const killCommand = os.platform() === 'win32' ? `taskkill /PID ${pid} /F` : `kill -9 ${pid}`;
      exec(killCommand, (killErr, killStdout, killStderr) => {
        if (killErr || killStderr) return reject(`Error: ${killErr || killStderr}`);
        resolve(`Process on port ${port} killed successfully.`);
      });
    });
  });
};
